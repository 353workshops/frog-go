package main

import "fmt"

func main() {
	s1 := []int{1, 2, 3, 4, 5}
	s2 := s1[1:3] // slicing notation, half-open range [)
	fmt.Println("s1", s1)
	fmt.Println("s2", s2)
	s2[0] = 100
	fmt.Println("s1", s1, "len", len(s1), "cap", cap(s1))
	fmt.Println("s2", s2, "len", len(s2), "cap", cap(s2))
	s1 = append(s1, 6)
	s2[0] = 200
	fmt.Println("s1", s1)
	fmt.Println("s2", s2)

	var s []int
	for i := 0; i < 100; i++ {
		s = appendInt(s, i)
	}

	fmt.Println(concat([]string{"A", "B"}, []string{"C"}))
	// [A B C]

	var a [3]int // a is an array (not slice)
	fmt.Printf("%v of %T\n", a, a)
}

func concat(s1, s2 []string) []string {
	// TODO: No for loops
	s := make([]string, len(s1)+len(s2))
	copy(s, s1)
	copy(s[len(s1):], s2)
	return s
}

func appendInt(s []int, val int) []int {
	if len(s) < cap(s) {
		s = s[:len(s)+1]
	} else {
		size := cap(s)*2 + 1
		fmt.Println(cap(s), "->", size)
		s1 := make([]int, size)
		copy(s1, s)
		s = s1[:len(s)+1]
	}
	s[len(s)-1] = val
	return s
}
