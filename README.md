# Go Workshop

JFrog ∴  2022 <br />

Miki Tebeka <i class="far fa-envelope"></i> [miki@353solutions.com](mailto:miki@353solutions.com), <i class="fab fa-twitter"></i> [@tebeka](https://twitter.com/tebeka), <i class="fab fa-linkedin-in"></i> [mikitebeka](https://www.linkedin.com/in/mikitebeka/), <i class="fab fa-blogger-b"></i> [blog](https://www.ardanlabs.com/blog/)  

#### Shameless Plugs

- [Go Brain Teasers](https://pragprog.com/titles/d-gobrain/go-brain-teasers/)
- [LinkedIn Learning](https://www.linkedin.com/learning/instructors/miki-tebeka)

---

[Final Exercise](_extra/dld.md)

---
# Day 2

## Agenda

- Testing
- Performance optimization
- Managing dependencies
- Code structure
- Configuration
- HTTP servers
- Logging & metrics

- [nlp Project](nlp)


[Terminal Log](_extra/day-2.log)

## Exercises

Convert [nlp](nlp) to a project:
- Write tests for `Tokenize`, use cases from [tokenize_cases.yml](nlp/testdata/tokenize_cases.yml)
- Document exported functions
- Use `stemmer.Stem` in `Tokenize`
- Write HTTP server with the following endpoints:
    - `POST /tokenize` that will return JSON with tokens for text in request body
    - `GET /stem/<word>` will return word stem as text
- Add option to configure address to listen on
- Add logging with `go.uber.org/zap`
- Add metrics with `expvar` for number of calls to `/tokenize`
- Find a way to make `Tokenize` run faster



## Links

- [Google Go Style Guide](https://google.github.io/styleguide/go/)
- Reading
    - [Ultimate Go Notebook](https://www.ardanlabs.com/ultimate-go-notebook/)
    - [Effective Go](https://go.dev/doc/effective_go)
- Go Security
    - [OWASP on Go](https://github.com/OWASP/Go-SCP)
    - [Black Hat Go](https://www.amazon.com/Black-Hat-Go-Programming-Pentesters/dp/1593278659)
    - [Security with Go](https://www.amazon.com/Security-Go-Explore-Golang-services/dp/1788627911)
- Performance Optimization
    - [Rules of Optimization Club](https://wiki.c2.com/?RulesOfOptimizationClub)
    - [benchstat](https://pkg.go.dev/golang.org/x/perf/cmd/benchstat) - Compare benchmark runs
    - [High Performance Go Workshop](https://dave.cheney.net/high-performance-go-workshop/dotgo-paris.html) by Dave Cheney
- Configuration
    - [conf](https://pkg.go.dev/github.com/ardanlabs/conf/v3) by Ardan Labs
    - [viper](https://github.com/spf13/viper) & [cobra](https://github.com/spf13/cobra)
- Logging 
    - Built-in [log](https://pkg.go.dev/log/)
    - [uber/zap](https://pkg.go.dev/go.uber.org/zap)
    - [logrus](https://github.com/sirupsen/logrus)
- Metrics
    - Built-in [expvar](https://pkg.go.dev/expvar/)
    - [Open Telemetry](https://opentelemetry.io/)
    - [Prometheus](https://pkg.go.dev/github.com/prometheus/client_golang/prometheus)
- [Go Code Review Comments](https://github.com/golang/go/wiki/CodeReviewComments)
- [Tutorial: Getting started with multi-module workspaces](https://go.dev/doc/tutorial/workspaces)
- [Example Project Structure](https://github.com/ardanlabs/service)
- [How to Write Go Code](https://go.dev/doc/code.html)
- Documentation
    - [Godoc: documenting Go code](https://go.dev/blog/godoc)
    - [Testable examples in Go](https://go.dev/blog/examples)
    - [Go documentation tricks](https://godoc.org/github.com/fluhus/godoc-tricks)
    - [gob/doc.go](https://github.com/golang/go/blob/master/src/encoding/gob/doc.go) of the `gob` package. Generates [this documentation](https://pkg.go.dev/encoding/gob/)
    - `go install golang.org/x/pkgsite/cmd/pkgsite@latest` (require go 1.18)
    - `pkgsite -http=:8080` (open browser on http://localhost:8080/${module name})
- [Out Software Dependency Problem](https://research.swtch.com/deps) - Good read on dependencies by Russ Cox
- Linters (static analysis)
    - [staticcheck](https://staticcheck.io/)
    - [gosec](https://github.com/securego/gosec) - Security oriented
    - [golang.org/x/tools/go/analysis](https://pkg.go.dev/golang.org/x/tools/go/analysis) - Helpers to write analysis tools (see [example](https://arslan.io/2019/06/13/using-go-analysis-to-write-a-custom-linter/))
- Testing
    - [testing](https://pkg.go.dev/testing/)
    - [testify](https://pkg.go.dev/github.com/stretchr/testify) - Many test utilities (including suites & mocking)
    - [Tutorial: Getting started with fuzzing](https://go.dev/doc/tutorial/fuzz)
        - [testing/quick](https://pkg.go.dev/testing/quick) - Initial fuzzing library
    - [test containers](https://golang.testcontainers.org/)
- HTTP Servers
    - [net/http](https://pkg.go.dev/net/http/)
    - [net/http/httptest](https://pkg.go.dev/net/http/httptest)
    - [gorilla/mux](https://github.com/gorilla/mux) - HTTP router with more frills
    - [chi](https://github.com/go-chi/chi) - A nice web framework

---

# Day 1

## Agenda

- Strings & Unicode
    - [banner.go](banner/banner.go)
- Understanding slices
    - [slices.go](slices/slices.go)
    - [median.go](median/median.go)
- Error handling
    - [kill_server.go](kill_server/kill_server.go)
- Sturcts methods & interfaces
    - [game.go](game/game.go)
    - [empty_iface.go](empty_iface/empty_iface.go)
    - A look at generics:  [stats.go](stats/stats.go)
- Concurrency with goroutines and channels
    - [go_chan.go](go_chan/go_chan.go)
    - Using context for timeouts and cancellations

## Exercises

- [banner.go](banner/banner.go)
- [median.go](median/median.go)
- [kill_server.go](kill_server/kill_server.go)
- [game.go](game/game.go)
- [dl_size.go](dl_size/dl_size.go)
    
[Terminal Log](_extra/day-1.log)

## Exercises

- Finish `dl_size`
- Read & understand the [sort package examples](https://pkg.go.dev/sort#example-package-SortKeys)


## Links

- [GopherCon Israel](https://www.gophercon.org.il/) - Submit a talk or sponsor
- [Go Proverbs](https://go-proverbs.github.io/)
- [Tracing Programs with Go](https://www.youtube.com/watch?v=mjixFKO-IdM) by Bill Kennedy
- [Uber Go style guide](https://github.com/uber-go/guide/blob/master/style.md)
- [Computer latency at human scale](https://twitter.com/jordancurve/status/1108475342468120576/photo/1)
- Concurrency
    - [Concurrency is not Parallelism](https://www.youtube.com/watch?v=cN_DpYBzKso) by Rob Pike
    - [Scheduling in Go](https://www.ardanlabs.com/blog/2018/08/scheduling-in-go-part2.html)
    - [Go Concurrency Patterns: Pipelines and cancellation](https://go.dev/blog/pipelines)
    - [Go Concurrency Patterns: Context](https://go.dev/blog/context)
    - [Channel Semantics](https://www.353solutions.com/channel-semantics)
    - [Curious Channels](https://dave.cheney.net/2013/04/30/curious-channels)
- [Tutorial: Getting started with generics](https://go.dev/doc/tutorial/generics)
- Error Handling
    - [Defer, Panic and Recover](https://go.dev/blog/defer-panic-and-recover)
    - [errors](https://pkg.go.dev/errors/) package ([Go 1.13](https://go.dev/blog/go1.13-errors))
    - [pkg/errors](https://github.com/pkg/errors)
- Slices
    - [Slices](https://go.dev/blog/slices) & [Slice internals](https://go.dev/blog/go-slices-usage-and-internals) on the Go blog
    - [Slice tricks](https://github.com/golang/go/wiki/SliceTricks)
- Strings
    - [Unicode table](https://unicode-table.com/)
    - [strings](https://pkg.go.dev/strings/) package - string utilities
    - [Go strings](https://go.dev/blog/strings)

## Data & Other

- [Slices](_extra/slices.png)
- [Unicode](_extra/unicode.pdf)
- [Syllabus](_extra/syllabus.pdf)
