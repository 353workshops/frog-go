package main

import (
	"fmt"
	"log"
	"sort"
)

// numbers:
// int, int8, int16, int32, int64, uint8, ...
// float64, float32
// x := 0 // int
// x := 0.0 // float64
func main() {
	nums := []float64{3, 1, 2}
	m, err := median(nums)
	if err != nil {
		log.Fatal(err)
		/*
			log.Print(err)
			os.Exit(1)
		*/
	}
	fmt.Println(nums, "->", m) // 2

	nums = []float64{3, 1, 4, 2}
	m, err = median(nums)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(nums, "->", m) // 2.5
}

/*
	median returns median of values.

If values is empty - return error
Calculate median:
  - sort values
  - if odd number of values - return middle
  - if even number of values - return average of middles
*/
func median(values []float64) (float64, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("median of empty slice")
	}

	// Copy so we won't change values
	vals := make([]float64, len(values))
	copy(vals, values)

	sort.Float64s(vals)
	i := len(vals) / 2
	if len(vals)%2 == 1 {
		return vals[i], nil
	}
	m := (vals[i-1] + vals[i]) / 2
	return m, nil
}
