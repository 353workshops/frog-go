package main

import (
	"fmt"
	"time"
)

func main() {
	go fmt.Println("goroutine")
	fmt.Println("main")

	for i := 0; i < 3; i++ {
		// Solution 2: loop variable
		i := i
		go func() {
			fmt.Println(i) // i from line 14
		}()
		/* Solution 1: pass a parameter
		go func(n int) {
			fmt.Println(n)
		}(i)
		*/
		/*
			go func() {
				fmt.Println(i) // BUG: same i for all goroutines
			}()
		*/

	}

	time.Sleep(100 * time.Millisecond)

	ch := make(chan int)
	go func() {
		ch <- 7 // send
	}()
	val := <-ch // receive
	fmt.Println("val:", val)

	fmt.Println(sleepSort([]int{20, 30, 10})) // [10 20 30]

	go func() {
		for i := 0; i < 4; i++ {
			ch <- i
		}
		close(ch)
	}()

	for n := range ch {
		fmt.Println("range:", n)
	}
	/*
		for {
			n, ok := <- ch
			if !ok {
				break
			}
			// user code
		}
	*/
	n := <-ch
	fmt.Println("closed:", n)
	n, ok := <-ch
	fmt.Println("closed:", n, ok)
	// ch <- 7 // panic
}

/*
	sleepSort

For every value "n" in values, spin a goroutine that will
  - sleep n milliseconds
  - send n over a channel

Collect all values from the channel to a slice and return it
*/
func sleepSort(values []int) []int {
	ch := make(chan int)
	for _, n := range values {
		n := n
		go func() {
			time.Sleep(time.Duration(n) * time.Millisecond)
			ch <- n
		}()

	}

	var out []int
	//for i := 0; i < len(values); i++ {
	for range values {
		n := <-ch
		out = append(out, n)
	}
	return out
}

/* Channel semantics
- Send/receive will block until opposite action (*)
	- Buffered channel of size "n" has "n" unblocking sends
- Send/receive on a nil channel will block forever
- Receive from a closed channel will return zero value without blocking
- Send/close to a closed channel will panic
*/
