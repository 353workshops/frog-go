package main

import "fmt"

func main() {
	// var i interface{}
	var i any // go >= 1.18

	i = 7
	fmt.Println("i", i)

	i = "Hi"
	fmt.Println("i", i)

	s := i.(string) // type assertion
	fmt.Println("s:", s)

	// n := i.(int) // panic
	n, ok := i.(int) // comma, ok
	if !ok {
		fmt.Println("not an int")
	} else {
		fmt.Println("n:", n)
	}

	f := float64(n) // conversion
	fmt.Println("f", f)

	/*
		s1 := []int{1, 2, 3}
		s2 := []any(s1)
		fmt.Println(s2)
	*/

}
