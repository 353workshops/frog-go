package nlp

import (
	"regexp"
	"strings"

	"git.jfrog.com/nlp/stemmer"
)

var (
	wordRe = regexp.MustCompile(`[a-z]+`)
	// wordRe *regexp.Regexp
)

/*
func init() {
	var err error
	wordRe, err = regexp.Compile(`[a-zA-Z]+`)
	if err != nil {
		panic(err)
	}
}
*/

// Tokenize returns tokens (lower case) found in text.
func Tokenize(text string) []string {
	text = strings.ToLower(text)
	words := wordRe.FindAllString(text, -1)
	// 75% of texts have 10 or less tokens
	// var tokens []string
	tokens := make([]string, 0, 10)
	for _, tok := range words {
		tok = stemmer.Stem(tok)
		if tok == "" {
			continue
		}
		tokens = append(tokens, tok)
	}
	return tokens
}
