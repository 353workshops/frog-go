package stemmer

import (
	"strings"
)

var (
	// works -> work
	// working -> work
	// worked -> work
	suffixes = []string{"ed", "ing", "s"}
)

// Stem returns the stemmed version of word
func Stem(word string) string {
	for _, suffix := range suffixes {
		if strings.HasSuffix(word, suffix) {
			return word[:len(word)-len(suffix)]
		}
	}
	return word
}
