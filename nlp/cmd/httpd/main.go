package main

import (
	"context"
	"encoding/json"
	"expvar"
	"fmt"
	"io"
	"log"
	"net/http"
	_ "net/http/pprof" // will install handlers under /debug/pprof (security risk)
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"

	"git.jfrog.com/nlp"
	"git.jfrog.com/nlp/stemmer"
)

var (
	tokCalls = expvar.NewInt("tokenize.calls")
)

// configuration: defaults < config file < environment < command line
// config file: yaml, toml
// environment: os.Getenv
// command line: flag

// cobra + viper
// super important: validate configuration
// port: must be number, 0 - 65535

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	expvar.NewString("nlp.version").Set("1.2.3")
	/* built-in router
	- path ends with / - prefix match
	- otherwise - exact match
	*/
	/*
		http.HandleFunc("/health", healthHandler)
		h := addLogging(http.HandlerFunc(tokenizeHandler))
		// http.HandleFunc("/tokenize", tokenizeHandler)
		http.Handle("/tokenize", h)
	*/
	r := mux.NewRouter()
	r.HandleFunc("/health", healthHandler).Methods(http.MethodGet)
	h := addLogging(http.HandlerFunc(tokenizeHandler))
	r.Handle("/tokenize", h).Methods(http.MethodPost)
	r.HandleFunc("/stem/{word}", stemHandler).Methods(http.MethodGet)
	// r.HandleFunc("/debug/pprof/profile", pprof.Profile())

	http.Handle("/", r)

	if err := http.ListenAndServe(":8080", nil); err != nil {
		// if err := http.ListenAndServe(":8080", r); err != nil {
		log.Fatalf("error: %s", err)
	}
}

func stemHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	word := vars["word"]
	if word == "" {
		http.Error(w, "missing word", http.StatusBadRequest)
		return
	}

	fmt.Fprintln(w, stemmer.Stem(word))
}

// It's recommended to have specific type for your context keys
// to avoid collision with other packages
type ctxKeyType int

var IDCtxKey = ctxKeyType(7)

// middleware example
func addLogging(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		id := uuid.NewString()
		ctx := context.WithValue(r.Context(), IDCtxKey, id)
		r = r.Clone(ctx)

		h.ServeHTTP(w, r)
		duration := time.Since(start)
		log.Printf("[%s] %s %s took %v", id, r.Method, r.URL.Path, duration)
	}

	return http.HandlerFunc(fn)

}

/*
	Exercise:

Write a tokenizeHandler that will read the text from r.Body and
return JSON with tokens.

$ curl -d"Who's on first?" http://localhost:8080/tokenize
{"tokens": ["who", "on", "first"]}
*/
func tokenizeHandler(w http.ResponseWriter, r *http.Request) {
	tokCalls.Add(1)
	// Step 1: Get & validate
	if r.Method != http.MethodPost {
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}

	data, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "can't read", http.StatusBadRequest)
		return
	}

	text := string(data)
	// TODO: Valid UTF-8?

	// Step 2: Work
	tokens := nlp.Tokenize(text)

	// Step 3: Output
	resp := map[string]any{
		"tokens": tokens,
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		// Can't send error to client
		log.Printf("ERROR: can't write JSON - %s", err)
	}

}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	resp := map[string]any{
		"error": nil,
		"time":  time.Now().UTC(),
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		// Can't send error to client
		log.Printf("ERROR: can't write JSON - %s", err)
	}
}
