package nlp

import "testing"

var (
	// 75% of text in corpus have less than 9 tokens
	text = "Design the architecture, name the components, document the details."
)

func BenchmarkTokenize(b *testing.B) {
	for i := 0; i < b.N; i++ {
		// Tokenize(text) // might be optimized away
		tokens := Tokenize(text)
		if size := len(tokens); size != 9 {
			b.Fatal(size)
		}
	}
}
