package nlp

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

func TestTokenize(t *testing.T) {
	// setup: call a function
	// teardown: defer or t.Cleanup
	text := "What's on second?"
	expected := []string{"what", "on", "second"}
	tokens := Tokenize(text)
	// if tokens != expected { // can't compare slices in Go
	/* before testify
	if !reflect.DeepEqual(tokens, expected) {
		t.Fatalf("%q: expected: %#v, got %#v", text, expected, tokens)
	}
	*/
	require.Equal(t, expected, tokens)
}

/* Exercise
- Read test cases from tokenize_cases.yml
- Use testify in TestTableTokenize
*/

type tokenizeCase struct {
	Text     string
	Expected []string `yaml:"tokens"`
	Name     string
}

func loadTokenizeCases(t *testing.T, fileName string) []tokenizeCase {
	file, err := os.Open(fileName)
	require.NoError(t, err, "open file")
	defer file.Close()

	dec := yaml.NewDecoder(file)
	var cases []tokenizeCase
	err = dec.Decode(&cases)
	require.NoError(t, err, "decode")
	return cases
}

func TestTableTokenize(t *testing.T) {
	testCases := loadTokenizeCases(t, "testdata/tokenize_cases.yml")
	/*
		var testCases = []struct {
			text     string
			expected []string
		}{
			{"Who's on first?", []string{"who", "s", "on", "first"}},
			{"What's on second?", []string{"what", "s", "on", "second"}},
			{"", nil},
		}
	*/

	for _, tc := range testCases {
		name := tc.Name
		if name == "" {
			name = tc.Text
		}
		t.Run(name, func(t *testing.T) {
			tokens := Tokenize(tc.Text)
			require.Equal(t, tc.Expected, tokens)
		})
	}
}

func FuzzTokenize(f *testing.F) {
	f.Add("Who's on first?")
	fn := func(t *testing.T, text string) {
		t.Logf("TEXT: %s", text)
		tokens := Tokenize(text)
		lText := strings.ToLower(text)
		for _, tok := range tokens {
			require.Contains(t, lText, tok)
		}
	}
	f.Fuzz(fn)
}
