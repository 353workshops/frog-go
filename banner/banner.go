package main

import (
	"fmt"
	"log"
	"unicode/utf8"
)

func main() {
	banner("Go", 6)
	banner("G☺", 6)

	example()
}

func example() {

	/*
		byte = uint8
		rune = int32 (code point/character)
	*/
	for i := range "G☺!" { // indices
		fmt.Println(i)
	}
	for i, c := range "G☺!" { // index + value
		fmt.Println(i, c)
	}

	a, b := 1, "1"
	log.Printf("a=%#v, b=%#v", a, b)

	/*
		var r string
		r = Reade // won't compile
	*/
	fmt.Println(Reader)
}

type Role string

const Reader Role = "reader"

/*
Print a banner for text with given width

For example: banner("Go", 6) should print:

		Go
	  ------
*/
func banner(text string, width int) {
	/*
		var padding int
		padding = (width - len(text)) / 2
	*/
	// padding := (width - len(text)) / 2
	padding := (width - utf8.RuneCountInString(text)) / 2
	for i := 0; i < padding; i++ {
		fmt.Print(" ")
	}
	fmt.Println(text)
	for i := 0; i < width; i++ {
		fmt.Print("-")
	}
	fmt.Println()
}
