package main

import (
	"errors"
	"fmt"
	"log"
	"os"
)

func main() {
	// kill_server/server.pid
	if err := killServer("server.pid"); err != nil {
		e := err
		for e != nil {
			fmt.Println("ERROR:", e)
			e = errors.Unwrap(e)
		}
		log.Fatalf("error: %s", err)
	}
}

/*
killServer kill server from pid written in pidFile

Read pid from the file as integer.
Print "killing <pid>" instead of killing the server
Remove pidFile after kill

Think about error handling.
*/
func killServer(pidFile string) error {
	// idiom: acquire a resource, check for error, defer release
	file, err := os.Open(pidFile)
	if err != nil {
		return err
	}
	defer func() {
		if err := file.Close(); err != nil {
			log.Printf("warning: can't close %q - %s", pidFile, err)
		}
	}()

	var pid int
	if _, err := fmt.Fscanf(file, "%d", &pid); err != nil {
		return fmt.Errorf("%q - bad pid (%w)", pidFile, err)
	}

	fmt.Printf("killing %d\n", pid) // simulate kill
	if err := os.Remove(pidFile); err != nil {
		log.Printf("warning: can't delete %q - %s", pidFile, err)
	}
	return nil
}
