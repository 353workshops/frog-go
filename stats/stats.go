package main

import "fmt"

func MaxInts(values []int) (int, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("MaxInts on empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}
	return m, nil
}

func MaxFloat64s(values []float64) (float64, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("MaxFloat64s on empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}
	return m, nil
}

type Comparable interface {
	// ~ means anything that is a int underneath
	~int | float64 | int8 | string
}

// type constraint
// func Max[T int | float64 | int8](values []T) (T, error) {
func Max[T Comparable](values []T) (T, error) {
	if len(values) == 0 {
		var zero T
		return zero, fmt.Errorf("Max on empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}
	return m, nil

}

type Role int

const (
	Reader Role = iota + 1
	Writer
	Admin
)

func main() {
	iVals := []int{3, 1, 2}
	// fmt.Println(MaxInts(iVals))
	fmt.Println(Max(iVals))

	fVals := []float64{3, 1, 2}
	// fmt.Println(MaxFloat64s(fVals))
	fmt.Println(Max(fVals))

	rVals := []Role{Reader, Admin}
	fmt.Println(Max(rVals))
}
