package main

import (
	"fmt"
	"log"
)

func main() {
	// var p1 Player
	// p1 := Player{"Parzival", 10, 20}
	/*
			p1 := Player{
				Name: "Parzival",
				Y:    20,
				//X:    10,
			}
		// fmt.Println(p1)
		p1, err := NewPlayer("Parzival", 20, 30)
		if err != nil {
			log.Fatalf("error: %s", err)
		}
		fmt.Printf("p1: %#v\n", p1)
		p1.Move(200, 300)
		fmt.Printf("p1: (move) %#v\n", p1)
	*/
	p1, err := NewPlayer("Parzival", 100, 200)
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Println("p1", p1)

	c1, err := NewCar("A3CH", 50, 40)
	if err != nil {
		log.Fatalf("error: %s", err)
	}

	items := []Mover{p1, &c1}
	moveAll(items, 0, 0)
	for _, m := range items {
		fmt.Println(m)
	}

	fmt.Println("copper:", Copper)

	p1.Found(Crystal)
	p1.Found(Crystal)
	fmt.Println(p1.Keys)           // [crystal]
	fmt.Println(p1.Found(Key(21))) // error...
}

/* Exercise:
- Add Keys field to player with is a slice of Key
- Add "Found(k Key) error" method to Player
	- It should err if k is not known
	- It should add a key only once (no duplicates)
*/

func (p *Player) Found(k Key) error {
	if !k.Valid() {
		return fmt.Errorf("invalid key: %v", k)
	}

	if !p.hasKey(k) {
		p.Keys = append(p.Keys, k)
	}
	return nil
}

func (p *Player) hasKey(k Key) bool {
	for _, k1 := range p.Keys {
		if k == k1 {
			return true
		}
	}
	return false
}

type Key byte

const (
	Copper Key = iota + 1
	Jade
	Crystal
	invalidKey // must be last
)

func (k Key) Valid() bool {
	return k > 0 && k < invalidKey
}

// implements the fmt.Stringer interface
func (k Key) String() string {
	switch k {
	case Copper:
		return "copper"
	case Jade:
		return "jade"
	case Crystal:
		return "crystal"
	}
	return fmt.Sprintf("<Key %d>", k)
}

/*
	TODO Player should have

- Name
- X & Y (location)
- Keys
*/
type Player struct {
	Name string
	X    int
	Y    int
	Keys []Key
}

const (
	maxX = 1000
	maxY = 600
)

// Rule of thumb: accept interfaces, return types

// func NewPlayer(name string, x, y int) Player {
// func NewPlayer(name string, x, y int) *Player {
// func NewPlayer(name string, x, y int) (Player, error) {
func NewPlayer(name string, x, y int) (*Player, error) {
	if name == "" {
		return nil, fmt.Errorf("empty name")
	}

	if x < 0 || x > maxX || y < 0 || y > maxY {
		err := fmt.Errorf("%d/%d of of bounds for %d/%d", x, y, maxX, maxY)
		return nil, err
	}

	p := Player{
		Name: name,
		X:    x,
		Y:    y,
	}
	return &p, nil // The go compiler does escape analysis and allocate p on heap
}

// Move should move the player to x, y
// p is called "the receiver"
func (p *Player) Move(x, y int) {
	p.X = x
	p.Y = y
}

/*
	TODO

Car should have
- License
- X
- Y
*/
type Car struct {
	License string
	X       int
	Y       int
}

func NewCar(license string, x, y int) (Car, error) {
	c := Car{
		License: license,
		X:       x,
		Y:       y,
	} // TODO: Validate
	return c, nil
}

// Move should move the car to x, y
func (c *Car) Move(x, y int) {
	c.X = x
	c.Y = y
}

type Mover interface {
	Move(int, int)
}

func moveAll(ms []Mover, x, y int) {
	for _, m := range ms {
		m.Move(x, y)
	}
}
